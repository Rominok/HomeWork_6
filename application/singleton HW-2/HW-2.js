/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()

      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

const government = {
  laws: [],
  budget: 1000000,
  citizensSatisfactions: 0,

  addLaw: (id, name, description) =>{
    let law = {id, name, description};
  //  return{
      laws.push(law);
      citizensSatisfactions -= 10;
  //  }
  }
  readConstitution: () =>{
    laws.map((e)=>{
      console.log(e);
    });
  }
  showBudget: () =>{
    console.log(budget);
  }
  showValueOfIncome: ()=>{
    console.log(citizensSatisfactions);
  }
  celebrateFeast: ()=>{
  //  return{
      budget -= 5000;
      citizensSatisfactions += 5;
  //  }
  }

  Object.freeze(government);

  export default government;


}
